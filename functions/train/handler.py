import json
import os
from typing import Dict

import boto3

from functions.train.common import get_s3_key_as_dataframe, put_model_to_s3, Preprocessor,ModelTrainer

MODEL_BUCKET = 'retail-tcns-data-bucket-s3-dev'
IS_DEPLOYED = False
if os.getenv('IS_OFFLINE') != 'true':  # Leverage built-in sls offline env var (this could change)
    IS_DEPLOYED = True

if IS_DEPLOYED:
    s3 = boto3.client('s3')
else:
    print("Running non deployed, using sls-s3-local configuration.")
    s3 = boto3.client('s3',
                      endpoint_url='http://localhost:4569',
                      aws_access_key_id='S3RVER',
                      aws_secret_access_key='S3RVER')


def trainer(sqs_batch, context) -> Dict:
    # right now only one record per batch but still need to process
    # print(sqs_batch)
    for record in sqs_batch['Records']:
        payload = json.loads(record['body'])
        s3_key_training_data = payload['s3_key']
        pandas_df = get_s3_key_as_dataframe(s3, MODEL_BUCKET, s3_key_training_data)

        values, covariates = Preprocessor(pandas_df).preprocess()
        mt = ModelTrainer(values, covariates)
        mt.train(train_val_split=0.8)

        # save model to s3
        model_s3_key = s3_key_training_data.replace('training_data', 'trained_model')
        put_model_to_s3(mt.model, s3, MODEL_BUCKET, model_s3_key)

    return {'message': 'success'}
