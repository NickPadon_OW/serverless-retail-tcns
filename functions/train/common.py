from datetime import date
from io import BytesIO
from typing import Tuple

import numpy as np
import pandas as pd
import torch
from darts import TimeSeries
from darts.dataprocessing.transformers import Scaler
from darts.models import TCNModel
from darts.utils.timeseries_generation import datetime_attribute_timeseries


def get_s3_key_as_dataframe(s3_client, s3_bucket: str, s3_key: str) -> pd.DataFrame:
    """
    Reads parquet file at the target key. If s3_key is not a parquet file, raises
    NotImplemented
    Args:
        s3_client: boto3 s3 client
        s3_bucket: bucket name
        s3_key: s3 key, ending in parquet.

    Returns: pandas dataframe
    """
    if not s3_key.endswith('parquet'):
        raise NotImplementedError("Only supports read of parquet files")
    # read parquet object from s3 and load into pandas dataframe
    response = s3_client.get_object(Bucket=s3_bucket, Key=s3_key)
    response_body = response['Body']  # streaming body
    result_bytes = BytesIO(response_body.read())  # force read of entire stream
    return pd.read_parquet(result_bytes, engine='pyarrow')


def put_model_to_s3(model, s3_client, s3_bucket: str, s3_key: str):
    buffer = BytesIO()
    torch.save(model, buffer)
    s3_client.put_object(Bucket=s3_bucket, Key=s3_key, Body=buffer.getvalue())


class Preprocessor:
    def __init__(self, training_data: pd.DataFrame):
        self.training_df = training_data

    def preprocess(self) -> Tuple[TimeSeries, TimeSeries]:
        # make sure the data is all on the same frequency and has a dt index
        training_df = self.training_df[['date', 'unit_sales']].copy()
        training_df['date'] = pd.to_datetime(training_df['date'])
        training_df = training_df.set_index('date')
        training_df_clean = training_df.resample('D').asfreq().fillna(0).copy()
        ts = TimeSeries.from_dataframe(df=training_df_clean, freq='D')

        # scale the data from 0-1
        scaler = Scaler()
        ts_transform = scaler.fit_transform(ts)  # scale the whole time series not caring about train/val split...

        # Create some dumb covariates (weekday flags and month flags)
        day_series = datetime_attribute_timeseries(ts_transform, attribute='weekday', one_hot=True)
        scaler_day = Scaler()
        day_series_transform = scaler_day.fit_transform(day_series)

        month_series = datetime_attribute_timeseries(ts_transform, attribute='month', one_hot=True)
        scaler_month = Scaler()
        month_series_transform = scaler_month.fit_transform(month_series)

        all_covariates_transform = day_series_transform.stack(month_series_transform)

        return ts_transform, all_covariates_transform


class ModelTrainer:

    @property
    def model_name(self) -> str:
        return '-'.join([str(date.today()), 'pytorch-model', str(np.random.randint(1000, 9999))])

    def __init__(self, target_series: TimeSeries, covariates: TimeSeries):
        self.target_series = target_series
        self.covariates = covariates
        self.model = TCNModel(
            input_chunk_length=30,
            output_chunk_length=5,
            kernel_size=4,
            num_filters=3,
            n_epochs=250,
            dropout=0.1,
            dilation_base=3,
            weight_norm=True,
            random_state=0,
            model_name=self.model_name
        )

    def train(self, train_val_split=0.8):
        train, val = self.target_series.split_after(train_val_split)
        train_covariates, val_covariates = self.covariates.split_after(train_val_split)
        self.model.fit(series=train,
                       covariates=train_covariates,
                       val_series=val,
                       val_covariates=val_covariates,
                       verbose=True)

    def evaluate(self, n=60):
        pass
