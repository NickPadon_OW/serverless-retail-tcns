import json
import os
from typing import Dict

import boto3

IS_DEPLOYED = False
QUEUE_NAME = 'models-to-train-sqs'
if os.getenv('IS_OFFLINE') != 'true':  # Leverage built-in sls offline env var (this could change)
    IS_DEPLOYED = True

if IS_DEPLOYED:
    sqs_client = boto3.client('sqs')
else:
    print("Running non deployed, using sqs-local configuration.")
    sqs_client = boto3.client('sqs',
                              endpoint_url='http://localhost:9324',
                              aws_access_key_id='root',
                              aws_secret_access_key='root')


def trigger_batch_train(event: Dict, context: Dict):
    # hard-coded-for now
    target_s3_training_data = ['data/training_data/264753/29.parquet',
                               'data/training_data/457424/6.parquet',
                               'data/training_data/1916577/20.parquet']
    queue_url_response = sqs_client.list_queues()
    print(queue_url_response)
    queue_url = queue_url_response['QueueUrls'][0]  # this would need to change
    for target_file in target_s3_training_data:
        body = {'s3_key': target_file}
        sqs_client.send_message(QueueUrl=queue_url, MessageBody=json.dumps(body))
