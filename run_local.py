import boto3
import click

from functions.train.common import get_s3_key_as_dataframe, put_model_to_s3, Preprocessor, ModelTrainer


@click.command()
def main():
    s3 = boto3.client('s3',
                      endpoint_url='http://localhost:4569',
                      aws_access_key_id='S3RVER',
                      aws_secret_access_key='S3RVER')

    # define variables
    parquet_local_path = 'data/training_data/264753/29.parquet'


    bucket = 'retail-tcns-data-bucket-s3-dev'
    s3_key = parquet_local_path

    # write object to s3 (from local filesystem...yes dumb)
        s3.upload_file(parquet_local_path, bucket, s3_key)

    # pandas_df = get_s3_key_as_dataframe(s3, bucket, s3_key)
    values, covariates = Preprocessor(pandas_df).preprocess()
    mt = ModelTrainer(values, covariates)
    mt.train(train_val_split=0.8)

    # save model to s3
    model_s3_key = s3_key.replace('training_data', 'trained_model')
    put_model_to_s3(mt.model, s3, bucket, model_s3_key)


if __name__ == '__main__':
    main()
