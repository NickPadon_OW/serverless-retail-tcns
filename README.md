The idea is:

1. Do some data engineering to separate massive retail data into consumable chunks (e.g. by store and by item through time) -> a store-item-time-series, which is stored in S3.
    1. Right now, the sample data in the repo is small enough for this to be done locally (4GB of data) and then copied to S3, although we could do something cooler if the data gets larger (e.g. AWS step functions). The main purpose of this repo is around distributed training and model saving.
1. For each store-item-time-series:
    1. Push the training data s3 location (the key of the file) to a queue
    1. For each queue payload, train a model using a serverless Lambda (all CPU-based, since Lambdas don't have great GPU support). Lambdas will scale horizontally.
    1. Alternatively, could do this with Express step functions and EC2 instances (maybe GPU support)
    1. Save each model result to S3
1. Run predict somewhere else (could also be done via the same queue -> lambda pattern)

This repo does the train and save on some locally created data.